function calcular(){
    cantidad = document.getElementById('cantidad').value;
    monedaO = document.getElementById('monedaOrigen').value;
    monedaD = document.getElementById('monedaDestino').value;
    let res = 0;

    if(monedaO == 1 && monedaD == 1){
        res = cantidad;

    }else if(monedaO == 2 && monedaD == 1){
        res = (cantidad * 19.85);
         

    }else if(monedaO == 3 && monedaD == 1){
        res = (cantidad * 26.79);
        

    }else if (monedaO == 4 && monedaD == 1){
        res = (cantidad * 19.65);
        

    }else if(monedaO == 1 && monedaD == 2){
        res = (cantidad / 19.85);
        

    }else if(monedaO == 2 && monedaD == 2){
        res = cantidad;
        

    }else if(monedaO == 3 && monedaD == 2){
        res = (cantidad / 1.36);
        

    }else if(monedaO == 4 && monedaD == 2){
        res = (cantidad / .99);
        

    }else if(monedaO == 1 && monedaD == 3){
        res = (cantidad / 26.79);
        

    }else if(monedaO == 2 && monedaD == 3){
        res = (cantidad * 1.36);
        

    }else if(monedaO == 3 && monedaD == 3){
        res = cantidad;
        

    }else if(monedaO == 4 && monedaD == 3){
        res = (cantidad * 1.37);
        

    }else if(monedaO == 1 && monedaD == 4){
        res = (cantidad * 20.04);
        

    }else if(monedaO == 2 && monedaD == 4){
        res = (cantidad * .99);
        

    }else if(monedaO == 3 && monedaD == 4){
        res = (cantidad / 1.37);
        

    }else if(monedaO == 4 && monedaD == 4){
        res = cantidad;
        

    }

    document.getElementById('subtotal').value = "$" + res.toFixed(2);
    document.getElementById('comision').value = "$" + (res - (res*0.03)).toFixed(2);

    return res;
}

function registro(res){
    let sub1 = document.getElementById('sub1');
    let com1 = document.getElementById('com1');
    let tot1 = document.getElementById('tot1');

    sub1.innerHTML=res.toFixed(2);
    com1.innerHTML=(res*0.03).toFixed(2);
    tot1.innerHTML=(res - (res*0.03)).toFixed(2); 

}

function borrar(){
    let sub1=document.getElementById('sub1');
    let com1=document.getElementById('com1');
    var tot1=document.getElementById('tot1');

    sub1.innerHTML="";
    com1.innerHTML="";
    tot1.innerHTML="";

}


